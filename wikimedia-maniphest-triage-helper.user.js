// ==UserScript==
// @name		Wikimedia Maniphest Triage helper
// @namespace   http://tampermonkey.net/
// @description	Adds links above the comment field for stock answer, plus some other things.
// @author		Andre Klapper <ak-47@gmx.net>
// @version		2022-12-31
// @match		https://phabricator.wikimedia.org/T*
// @match		https://phabricator.wikimedia.org/maniphest/task/edit/*
// @match		https://phabricator.wikimedia.org/maniphest/report/project/*
// @match		https://phabricator.wikimedia.org/file/query/*
// @match		https://phabricator.wikimedia.org/p/*
// @grant		none
// @copyright	Copyright Control by the authors, 2014 and later.
// @license		The contents of this file are dual-licensed under the Mozilla Public License 2.0 (MPL-2.0)
//              and the GNU General Public License (GPL-2.0) Version 2 or later.
// ==/UserScript==

/* THIS SCRIPT MIGHT NOT WORK AT ALL IN VIOLENTMONKEY ETC.:
https://github.com/violentmonkey/violentmonkey/issues/408
https://github.com/violentmonkey/violentmonkey/issues/173
https://bugzilla.mozilla.org/show_bug.cgi?id=1267027#c41 */

if (document.getElementsByClassName('phui-crumbs-view').length > 0) {
  document.getElementsByClassName('phui-crumbs-view')[0].setAttribute('style', "display: none;");
}

/***********************************************************************/
/* SETTINGS SECTION START - values must be 0 (disabled) or 1 (enabled) */
/***********************************************************************/
// Color projects for extensions which are deployed in green, and extensions being worked on in yellow
var setColorForExtensions = 1;
// use one-click stock answers (canned responses) above field to add new comment.
// If you want to add individual stock answers yourself, you have to do two things:
//    a) add an EventHandler function (like "FooClick") in the STOCK ANSWER DEFINITIONS SECTION
//       to define your changes when clicking the stock answer (add comment text, set bug resolution, etc)
//    b) a createItemInContainer() call in the ADDING THE STOCK ANSWERS section at the very end of this script
//       that adds the stock answer link and calls the EventHandler function that you defined under a)
var useStockAnswers = 1;

// Set number of values proposed in dropdowns:
// JX.TypeaheadSource.prototype.setMaximumResultCount(20);

/**********************************************************************************/
/* Additions to user homepages at https://phabricator.wikimedia.org/p/username/ : */
/**********************************************************************************/
if ((window.location.pathname.split( '/' )[1] == "p") && ((window.location.pathname.split( '/' )[2]).length > 0)) {
// Provide link to 'People Log Query' on user homepage, to quickly check for other Phab users with the same or similar IPs:
  addLinkSection("https://phabricator.wikimedia.org/people/logs/?users=" + (window.location.pathname.split( '/' )[2]), " [Query Phab User Logins] ", "User Activity", document.getElementsByClassName("phui-header-header")[0], "");
}

/*********************************************************************************************************************************/
/* Mark trusted people as green for latest manual file uploads at https://phabricator.wikimedia.org/file/query/wOJAFQjUK2Ql/#R : */
/*********************************************************************************************************************************/
if ((window.location.pathname.split( '/' )[1] == "file") && (window.location.pathname.split( '/' )[2] == "query")) {
  var uploaders_array=[];
  for (var pe=0; pe < (document.getElementsByClassName("phui-link-person").length); pe++) {
    uploaders_array[pe] = document.getElementsByClassName("phui-link-person")[pe].innerHTML;
  }
  var uploaders_array_containers=[];
  for (var k=0; k < (document.getElementsByClassName("phui-oi-frame").length); k++) {
    uploaders_array_containers[k] = document.getElementsByClassName("phui-oi-frame")[k];
  }
  for (var i=0; i<uploaders_array.length; i++) {
    if (
      (uploaders_array[i] == "XXXXXXXXX") |
      (uploaders_array[i] == "YYYYYYYYY")
    ) {
      uploaders_array_containers[i].style.background = 'linear-gradient(to right, #DEE7F8, #40FF40)';
    }
  }
}

/*********************************************************/
/* get some basic data from the bug report to use later: */
/*********************************************************/

// Find out whether we are in task View or in task Edit mode, they are pretty different:
var task_edit_mode = 0;
if (window.location.pathname.split( '/' )[3] == "edit") {
  task_edit_mode = 1;
}

var assignee = null;
var comment_textfield = null;

if ((task_edit_mode == 0) && ((document.getElementsByClassName('fa-square-o').length > 0) || (document.getElementsByClassName('fa-check-square-o').length > 0))) { // we are in task view

  /* Priority; only existing if task not resolved: */
  if (document.getElementsByClassName('fa-square-o').length > 0) {
    var priority = ((document.getElementsByClassName('fa-square-o')[0].parentElement.textContent).substring(document.getElementsByClassName('fa-square-o')[0].parentElement.textContent.indexOf(", ") + 2,document.getElementsByClassName('fa-square-o')[0].parentElement.textContent.length));
  }

  /* Assignee: */
  if ((document.getElementsByClassName('phui-curtain-panel')[0].textContent).substring(document.getElementsByClassName('phui-curtain-panel')[0].textContent.indexOf("Assigned To") + 11,document.getElementsByClassName('phui-curtain-panel')[0].textContent.length) == "None") { // no assignee set
    assignee = "None";
  }
  else {
    assignee = (document.getElementsByClassName('phui-curtain-panel')[0].textContent).substring(document.getElementsByClassName('phui-curtain-panel')[0].textContent.indexOf("Assigned To") + 11,document.getElementsByClassName('phui-curtain-panel')[0].textContent.length);
  }

  /* Reporter: */
  var reporter = document.getElementsByClassName('phui-curtain-panel')[1].querySelector('.phui-link-person').textContent;

  /* Products: */
  var product_array=[]; // collect string names of all the Projects for this task:
  for (var n=0; n < (document.getElementsByClassName("phui-curtain-panel")[2].getElementsByClassName("phui-tag-core").length); n++) {
    product_array[n] = document.getElementsByClassName("phui-curtain-panel")[2].getElementsByClassName("phui-tag-core")[n].textContent;
  }
  if (setColorForExtensions == 1) {
    var product_array_containers=[]; // collect containers of all the Projects for this task so we can color them:
    for (var p=0; p < (document.getElementsByClassName("phui-curtain-panel")[2].getElementsByClassName("phui-tag-core").length); p++) {
      product_array_containers[p] = document.getElementsByClassName("phui-curtain-panel")[2].getElementsByClassName("phui-tag-core")[p];
    }
  }
  /* Get the textfield to add comments: */
  if (document.getElementsByName("comment").length > 0) {
    comment_textfield = document.getElementsByName("comment")[0]; // must point to actual <textarea> id
  }
  else {
    window.alert("Either you are logged out, or the HTML ID of the comment field has changed and your local Phab Stock Answer user script needs updating.");
  }
}
else { // we are in task edit mode:
  comment_textfield = document.getElementsByName("description")[0];
}

/* Bug number: */
if (document.getElementsByClassName('phui-crumb-name').length > 1) {
  var taskId = document.getElementsByClassName('phui-crumb-name')[1].innerHTML;
}

// Check whether task is open or closed. As the top section describing the task
// status is rather hard to gather, check if a class 'status-oh-closed-dark'
// exists - if not, the ticket is open.
var task_status_is_open = 0;
if (document.getElementsByClassName('phui-header-status-dark').length == 0) {
  task_status_is_open = 1;
}

/* Get name of user who last changed the task status and who added goodfirsttask tag (for specific stock answers below): */
/* Won't always work due to autocollapsed history. */
var task_activity_array=[];
var last_user_changing_task_status;
var last_user_adding_goodfirsttask_tag;
for(var tc=0; tc<((document.getElementsByClassName('phui-timeline-title')).length); tc++) {
  task_activity_array[tc] = document.getElementsByClassName('phui-timeline-title')[tc].textContent;
}
var taal = task_activity_array.length;
while (taal--) {
    if (task_activity_array[taal].indexOf("added a project: good first task") > -1) {
        last_user_adding_goodfirsttask_tag = (task_activity_array[taal].split(" ")[0]);
    }
    if ((task_activity_array[taal].indexOf("added projects") > -1) && (task_activity_array[taal].indexOf("good first task") > -1)) {
        last_user_adding_goodfirsttask_tag = (task_activity_array[taal].split(" ")[0]);
    }
    if (task_activity_array[taal].indexOf("changed the task status from") > -1) {
        last_user_changing_task_status = (task_activity_array[taal].split(" ")[0]);
    }
    if (task_activity_array[taal].indexOf("reopened this task as") > -1) {
        last_user_changing_task_status = (task_activity_array[taal].split(" ")[0]);
    }
    // good_first_task already set by reporter?:
    if (last_user_adding_goodfirsttask_tag == null) {
        last_user_adding_goodfirsttask_tag = reporter;
    }
}


// Check access restrictions of task; mark as red in header if not entirely public
if ((task_edit_mode == 0) && (document.getElementsByClassName('policy-link').length > 0)) { // does not make sense in edit mode as not displayed
  if (document.getElementsByClassName('policy-link')[0].innerHTML != "Public") {
    (document.getElementsByClassName('policy-link')[0]).setAttribute('style', "background-color:#FF7777;");
  }
  else {
      (document.getElementsByClassName('policy-link')[0]).setAttribute('style', "background-color:#9be644;");
  }
}

/*** helper function to create container elements (e.g. <div>) that we can drop random text or links into
 *  @param ElementToCreate the element (e.g. a <div>) that we plan to put somewhere,
 *         MUST have been defined by calling document.createElement("foo") before
 *  @param place element after which this item is added
 *  @param ID a random ID parameter
 *  @param heading optional text header to display at the beginning of the element
 *  @param style CSS style for this item
 *  @return void
 */
function createContainer(ElementToCreate, place, ID, heading, style) {
  ElementToCreate.setAttribute('id', ID);
  ElementToCreate.appendChild(document.createTextNode(heading));
  ElementToCreate.setAttribute('style', style);
  place.parentNode.insertBefore(ElementToCreate, place);
}

/********************************************************************/
/***helper function that adds an <a> element in a place
 *  @param url URL of the link
 *  @param text the text to be displayed
 *  @param title text to be displayed for mouse over
 *  @param place element after which this item is added
 *  @param style CSS style for this item
 *  @return void
 */
function addLinkSection(url, text, title, place, style) {
  var link = document.createElement('a');
  link.setAttribute('href', url);
  link.setAttribute('title', title);
  link.setAttribute('style', style);
  link.appendChild(document.createTextNode(text));
  if(place.nextSibling != null) {
    place.parentNode.insertBefore(link, place.nextSibling);
   }
}

/*************************************************************************/
/* Color MW extension projects that are deployed on WMF servers in green */
/* (as per https://gerrit.wikimedia.org/r/gitweb?p=mediawiki/tools/release.git;a=blob;f=make-wmf-branch/default.conf ), */
/* extensions to get deployed at some point on WMF servers in yellow;    */
/* third party extensions not planned to get deployed on WMF in red      */
/*************************************************************************/
if ((setColorForExtensions == 1) && (product_array != null)) {
  for (var pr=0; i<product_array.length; pr++) {
      if ( /* green */
      (product_array[pr] == "MediaWiki-extensions-AbuseFilter") |
      (product_array[pr] == "MediaWiki-extensions-ActiveAbstract") |
      (product_array[pr] == "MediaWiki-extensions-AntiBot") |
      (product_array[pr] == "MediaWiki-extensions-AntiSpoof") |
      (product_array[pr] == "MediaWiki-extensions-Babel") |
      (product_array[pr] == "BetaFeatures") |
      (product_array[pr] == "MediaWiki-extensions-BetaFeatures") |
      (product_array[pr] == "MediaWiki-extensions-BounceHandler") |
      (product_array[pr] == "MediaWiki-extensions-Calendar") |
      (product_array[pr] == "MediaWiki-extensions-Capiunto") |
      (product_array[pr] == "MediaWiki-extensions-Campaigns") |
      (product_array[pr] == "MediaWiki-extensions-CategoryTree") |
      (product_array[pr] == "MediaWiki-extensions-CentralAuth") |
      (product_array[pr] == "MediaWiki-extensions-CentralNotice") |
      (product_array[pr] == "MediaWiki-extensions-CharInsert") |
      (product_array[pr] == "MediaWiki-extensions-CheckUser") |
      (product_array[pr] == "MediaWiki-extensions-CirrusSearch") |
      (product_array[pr] == "CirrusSearch") |
      (product_array[pr] == "MediaWiki-extensions-Cite") |
      (product_array[pr] == "Cite") |
      (product_array[pr] == "MediaWiki-extensions-CiteThisPage") |
      (product_array[pr] == "MediaWiki-extensions-CLDR") |
      (product_array[pr] == "MediaWiki-extensions-CleanChanges") |
      (product_array[pr] == "MediaWiki-extensions-ClientSide") |
      (product_array[pr] == "MediaWiki-extensions-CodeEditor") |
      (product_array[pr] == "MediaWiki-extensions-CodeMirror") |
      (product_array[pr] == "MediaWiki-extensions-Collection") |
      (product_array[pr] == "MediaWiki-extensions-CommonsMetadata") |
      (product_array[pr] == "MediaWiki-extensions-ConfirmEdit-(CAPTCHA-extension)") |
      (product_array[pr] == "MediaWiki-extensions-ContactPage") |
      (product_array[pr] == "MediaWiki-extensions-ContactPageFundraiser") |
      (product_array[pr] == "MediaWiki-extensions-ContentTranslation") |
      (product_array[pr] == "MediaWiki-extensions-Contest") |
      (product_array[pr] == "MediaWiki-extensions-ContributionReporting") |
      (product_array[pr] == "MediaWiki-extensions-ContributionTracking") |
      (product_array[pr] == "MediaWiki-extensions-CreditsSource") |
      (product_array[pr] == "MediaWiki-extensions-Dashiki") |
      (product_array[pr] == "MediaWiki-extensions-DataTypes") |
      (product_array[pr] == "MediaWiki-extensions-DataValues") |
      (product_array[pr] == "MediaWiki-extensions-Diff") |
      (product_array[pr] == "MediaWiki-extensions-Disambiguator") |
      (product_array[pr] == "MediaWiki-extensions-DismissableSiteNotice") |
      (product_array[pr] == "MediaWiki-extensions-DonationInterface") |
      (product_array[pr] == "MediaWiki-extensions-DoubleWiki") |
      (product_array[pr] == "MediaWiki-extensions-DynamicPageList") |
      (product_array[pr] == "MediaWiki-extensions-DynamicSidebar") |
      (product_array[pr] == "Echo") |
      (product_array[pr] == "MediaWiki-extensions-Echo") |
      (product_array[pr] == "MediaWiki-extensions-EventLogging") |
      (product_array[pr] == "MediaWiki-extensions-ExtensionDistributor") |
      (product_array[pr] == "MediaWiki-extensions-FeaturedFeeds") |
      (product_array[pr] == "MediaWiki-extensions-FlaggedRevs") |
      (product_array[pr] == "StructuredDiscussions") |
      (product_array[pr] == "MediaWiki-extensions-Flow") |
      (product_array[pr] == "MediaWiki-extensions-FormPreloadPostCache") |
      (product_array[pr] == "MediaWiki-extensions-FundraiserLandingPage") |
      (product_array[pr] == "MediaWiki-extensions-Gadgets") |
      (product_array[pr] == "MediaWiki-extensions-GeoCrumbs") |
      (product_array[pr] == "MediaWiki-extensions-GeoData") |
      (product_array[pr] == "MediaWiki-extensions-GettingStarted") |
      (product_array[pr] == "MediaWiki-extensions-GlobalBlocking") |
      (product_array[pr] == "GlobalCssJs") |
      (product_array[pr] == "MediaWiki-extensions-GlobalCssJs") |
      (product_array[pr] == "MediaWiki-extensions-GlobalPreferences") |
      (product_array[pr] == "GlobalProfile") |
      (product_array[pr] == "MediaWiki-extensions-GlobalUsage") |
      (product_array[pr] == "GlobalUserPage") |
      (product_array[pr] == "MediaWiki-extensions-GoogleNewsSitemap") |
      (product_array[pr] == "MediaWiki-extensions-Graph") |
      (product_array[pr] == "MediaWiki-extensions-GrowthExperiments") |
      (product_array[pr] == "MediaWiki-extensions-GuidedTour") |
      (product_array[pr] == "MediaWiki-extensions-GWToolset") |
      (product_array[pr] == "MediaWiki-extensions-ImageMap") |
      (product_array[pr] == "MediaWiki-extensions-ImageMetrics") |
      (product_array[pr] == "MediaWiki-extensions-InputBox") |
      (product_array[pr] == "MediaWiki-extensions-Insider") |
      (product_array[pr] == "MediaWiki-extensions-intersection") |
      (product_array[pr] == "MediaWiki-extensions-Interwiki") |
      (product_array[pr] == "MediaWiki-extensions-InterwikiSorting") |
      (product_array[pr] == "MediaWiki-extensions-Josa") |
      (product_array[pr] == "MediaWiki-extensions-JsonConfig") |
      (product_array[pr] == "MediaWiki-extensions-LabeledSectionTransclusion") |
      (product_array[pr] == "MediaWiki-extensions-LandingCheck") |
      (product_array[pr] == "MediaWiki-extensions-LdapAuthentication") |
      (product_array[pr] == "LiquidThreads") |
      (product_array[pr] == "MediaWiki-extensions-LiquidThreads") |
      (product_array[pr] == "MediaWiki-extensions-Listings") |
      (product_array[pr] == "MediaWiki-extensions-LocalisationUpdate") |
      (product_array[pr] == "MediaWiki-extensions-LoginNotify") |
      (product_array[pr] == "MediaWiki-extensions-MapSources") |
      (product_array[pr] == "MassMessage") |
      (product_array[pr] == "MediaWiki-extensions-MassMessage") |
      (product_array[pr] == "MediaWiki-extensions-Math") |
      (product_array[pr] == "MediaWiki-extensions-MultimediaViewer") |
      (product_array[pr] == "MediaWiki-extensions-MwEmbedSupport") |
      (product_array[pr] == "MediaWiki-extensions-MWSearch") |
      (product_array[pr] == "MediaWiki-extensions-MobileFrontend") |
      (product_array[pr] == "MediaWiki-extensions-NavigationTiming") |
      (product_array[pr] == "MediaWiki-extensions-Newsletter") |
      (product_array[pr] == "MediaWiki-extensions-NewUserMessage") |
      (product_array[pr] == "MediaWiki-extensions-Nuke") |
      (product_array[pr] == "MediaWiki-extensions-OATHAuth") |
      (product_array[pr] == "MediaWiki-extensions-OAuth") |
      (product_array[pr] == "MediaWiki-extensions-OpenSearchXml") |
      (product_array[pr] == "MediaWiki-extensions-OpenStackManager") |
      (product_array[pr] == "MediaWiki-extensions-PageCuration") |
      (product_array[pr] == "MediaWiki-extensions-PagedTiffHandler") |
      (product_array[pr] == "MediaWiki-extensions-PageImages") |
      (product_array[pr] == "PageViewInfo") |
      (product_array[pr] == "MediaWiki-extensions-ParserFunctions") |
      (product_array[pr] == "MediaWiki-extensions-PdfHandler") |
      (product_array[pr] == "MediaWiki-extensions-Petition") |
      (product_array[pr] == "MediaWiki-extensions-Poem") |
      (product_array[pr] == "MediaWiki-extensions-PoolCounter") |
      (product_array[pr] == "Popups") |
      (product_array[pr] == "MediaWiki-extensions-Popups") | // Hovercards
      (product_array[pr] == "MediaWiki-extensions-ProofreadPage") |
      (product_array[pr] == "MediaWiki-extensions-Quiz") |
      (product_array[pr] == "MediaWiki-extensions-RandomRootPage") |
      (product_array[pr] == "MediaWiki-extensions-RelatedArticles") |
      (product_array[pr] == "MediaWiki-extensions-RelatedSites") |
      (product_array[pr] == "MediaWiki-extensions-Renameuser") |
      (product_array[pr] == "MediaWiki-extensions-RSS") |
      (product_array[pr] == "MediaWiki-extensions-SandboxLink") |
      (product_array[pr] == "MediaWiki-extensions-ScanSet") |
      (product_array[pr] == "MediaWiki-extensions-Score") |
      (product_array[pr] == "MediaWiki-extensions-Scribunto") |
      (product_array[pr] == "MediaWiki-extensions-SearchExtraNS") |
      (product_array[pr] == "MediaWiki-extensions-SecureLinkFixer") |
      (product_array[pr] == "MediaWiki-extensions-SecurePoll") |
      (product_array[pr] == "MediaWiki-extensions-ShortUrl") |
      (product_array[pr] == "MediaWiki-extensions-SiteMatrix") |
      (product_array[pr] == "MediaWiki-extensions-SpamBlacklist") |
      (product_array[pr] == "MediaWiki-extensions-SubPageList") |
      (product_array[pr] == "MediaWiki-extensions-SubpageSortkey") |
      (product_array[pr] == "SyntaxHighlight") |
      (product_array[pr] == "TemplateData") |
      (product_array[pr] == "MediaWiki-extensions-TemplateSandbox") |
      (product_array[pr] == "TextExtracts") |
      (product_array[pr] == "MediaWiki-extensions-TextExtracts") |
      (product_array[pr] == "MediaWiki-extensions-Thanks") |
      (product_array[pr] == "Thanks") |
      (product_array[pr] == "MediaWiki-extensions-TimedMediaHandler") |
      (product_array[pr] == "MediaWiki-extensions-Timeline") |
      (product_array[pr] == "MediaWiki-extensions-TitleBlacklist") |
      (product_array[pr] == "MediaWiki-extensions-TitleKey") |
      (product_array[pr] == "MediaWiki-extensions-TocTree") |
      (product_array[pr] == "MediaWiki-extensions-TorBlock") |
      (product_array[pr] == "MediaWiki-extensions-Translate") |
      (product_array[pr] == "MediaWiki-extensions-TranslationNotifications") |
      (product_array[pr] == "MediaWiki-extensions-TrustedXFF") |
      (product_array[pr] == "MediaWiki-extensions-TwnMainPage") |
      (product_array[pr] == "MediaWiki-extensions-UniversalLanguageSelector") |
      (product_array[pr] == "MediaWiki-extensions-UploadsLink") |
      (product_array[pr] == "UploadWizard") |
      (product_array[pr] == "MediaWiki-extensions-UrlShortener") |
      (product_array[pr] == "MediaWiki-extensions-UserMerge") |
      (product_array[pr] == "Vector") |
      (product_array[pr] == "MediaWiki-extensions-VipsScaler") |
      (product_array[pr] == "MediaWiki-extensions-WikibaseClient") |
      (product_array[pr] == "MediaWiki-extensions-WikibaseRepository") |
      (product_array[pr] == "MediaWiki-extensions-WikidataClient") |
      (product_array[pr] == "MediaWiki-extensions-WikidataRepo") |
      (product_array[pr] == "MediaWiki-extensions-wikidiff2") |
      (product_array[pr] == "MediaWiki-extensions-WikiEditor") |
      (product_array[pr] == "MediaWiki-extensions-WikiHiero") |
      (product_array[pr] == "WikiLove") |
      (product_array[pr] == "MediaWiki-extensions-WikimediaBadges") |
      (product_array[pr] == "MediaWiki-extensions-WikimediaEvents") |
      (product_array[pr] == "MediaWiki-extensions-WikimediaIncubator") |
      (product_array[pr] == "MediaWiki-extensions-WikimediaMaintenance") |
      (product_array[pr] == "MediaWiki-extensions-WikimediaMessages")
      ) {
        product_array_containers[pr].style.background = 'linear-gradient(to right, #DEE7F8, #40FF40)';
      }
      else if ( /* yellow */
      (product_array[pr] == "MediaWiki-extensions-EasyTimeline") |
      (product_array[pr] == "MediaWiki-extensions-ExtraLanguageLink") |
      (product_array[pr] == "GlobalPreferences") |
      (product_array[pr] == "MediaWiki-extensions-Cards") |
      (product_array[pr] == "MediaWiki-extensions-OpenID") |
      (product_array[pr] == "PronunciationRecording") |
      (product_array[pr] == "MediaWiki-extensions-SemanticForms") |
      (product_array[pr] == "MediaWiki-extensions-Semantic-MediaWiki") |
      (product_array[pr] == "MediaWiki-extensions-SemanticResultFormats") |
      (product_array[pr] == "MediaWiki-extensions-TemplateWizard") |
      (product_array[pr] == "MediaWiki-extensions-TwitterCards")
      ) {
        product_array_containers[pr].style.background = 'linear-gradient(to right, #DEE7F8, #EFFF66)';
      }
      else if (product_array[pr].substring(0,21) == "MediaWiki-extensions-") {
        /* red but only for projects that are extensions and start with that name */
        product_array_containers[pr].style.background = 'linear-gradient(to right, #DEE7F8, #FFC0C0)';
      }
  }
}

/**********************************************************************/
/* create <div> container for stock answers above comment input field */
/* Only makes sense in non-edit mode as we don't have stockanswers to */
/* edit the initial description                                       */
/**********************************************************************/
if (useStockAnswers == 1 && comment_textfield != null) {
  var stockAnswersContainerDiv = document.createElement("div"); // most be defined here globally as we need it later to insert items in it
  createContainer(stockAnswersContainerDiv, comment_textfield, "stock_answers_div", '', "border:0px solid #000000; padding:0px; margin: 2px 0px 2px; text-align: left; background-color:#f4f4f4");
}

/**************************************************************************************/
/***helper function that creates new entries in custom <div>s, such as stock answers
 *  @param element HTML element to create (e.g. <span> or <div>)
 *  @param cclass class for the element to create
 *  @param ID some ID for the element to create
 *  @param text the text to be displayed
 *  @param clickHandler name of the EventHandler function; makes only sense if this is a stock response (function must have been defined before)
 *  @param style CSS style for this item
 *  @param place element after which this item is added
 *  @return void
 */
function createItemInContainer(element, cclass, ID, text, clickHandler, style, place) {
  var SpanContainer = document.createElement(element);
  SpanContainer.setAttribute('class', cclass);
  SpanContainer.setAttribute('id', ID);
  var SpanContainerText = document.createTextNode(text);
  SpanContainer.appendChild(SpanContainerText);
  place.appendChild(SpanContainer);
  if (style == '1') { // green
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#8ae234; color: #000000;");
  }
  else if (style == '10') { // dark green
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#4aa204; color: #FFFFFF;");
  }
  else if (style == '2') { // rose
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#f2a0f2; color: #000000;");
  }
  else if (style == '20') { // aubergine
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#ad7fa8; color: #FFFFFF;");
  }
  else if (style == '3') { // yellow
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#fce94f; color: #000000;");
  }
  else if (style == '4') { // orange
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#fcaf3e; color: #000000;");
  }
  else if (style == '5') { // brown
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#e9b97e; color: #000000;");
  }
  else if (style == '6') { // red
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#f56969; color: #000000;");
  }
  else if (style == '60') { // dark red
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#d54040; color: #ffffff;");
  }
  else if (style == '7') { // light blue
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#82afcf; color: #000000;");
  }
  else if (style == '70') { // dark blue
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#4475a4; color: #FFFFFF;");
  }
  else if (style == '99') { // black
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#2d2d2d; color: #FFFFFF;");
  }
  else /* if (style == '0') */ {
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; color: #000000;");
  }
  SpanContainer.addEventListener("click", clickHandler, true);
}

/**************************************************************************************/
/* helper function that adds reply text to the comment textarea, enables the CC checkbox
 * and sets keyboard focus to the "submit" button
 * (shamelessly partially adopted from bugzilla.gnome.org code at
 * /template/en/default/bug/edit.html.tmpl which is under a Mozilla Public License)
 * @param text text to add as a new comment
 * @param priority priority to set
 * @param status status to set (e.g. "new" or "resolved")
 * @param project project to associate (TODO: removing is probably too hard)
 * @return void
 */
function addTextToComment(text, priority, status, project) {
  var replytext = "";
  if (text && text != "") {
    text = text.split(/\r|\n/);
    replytext = "";
    for (var i=0; i < text.length; i++) {
      replytext += text[i] + "\n";
    }
  }

  if (replytext && replytext != "") {
    var textarea = comment_textfield;
    textarea.value += replytext;
  }

  return false;
}

/*******************************************************************************************************************/
/* STOCK ANSWER DEFINITIONS AND ADDING SECTION                                                                     */
/* PATTERN: createItemInContainer('span', 'class' 'someID', '[shownText]', ClickHandlerName, colorNumber, place);  */
/* The ClickHandlerName must be defined above as the function name!                                                */
/*******************************************************************************************************************/

var textThanks = "Hi @" + reporter + ", thanks for taking the time to report this! ";
var textThanksNew = "Hi @" + reporter + ", thanks for taking the time to report this and welcome to Wikimedia Phabricator!\n(If you have any questions about Phabricator itself, please see [mw:Phabricator/Help](https://www.mediawiki.org/wiki/Phabricator/Help).)\n";

/* // COLOR SCHEME EXAMPLES FOR BELOW:
  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv); // line breaks
  createItemInContainer('div', null, '', '1', null, 1, stockAnswersContainerDiv); // first triage
  createItemInContainer('div', null, '', '10', null, 10, stockAnswersContainerDiv); // first triage + stalled
  createItemInContainer('div', null, '', '2', null, 2, stockAnswersContainerDiv); // clean up
  createItemInContainer('div', null, '', '20', null, 20, stockAnswersContainerDiv); // clean up
  createItemInContainer('div', null, '', '3', null, 3, stockAnswersContainerDiv); // setting priority for first time
  createItemInContainer('div', null, '', '4', null, 4, stockAnswersContainerDiv); // WM-Site-requests; project specific answers
  createItemInContainer('div', null, '', '5', null, 5, stockAnswersContainerDiv); // Misc; Phab task answers
  createItemInContainer('div', null, '', '6', null, 6, stockAnswersContainerDiv); // revert
  createItemInContainer('div', null, '', '60', null, 60, stockAnswersContainerDiv); // revert and potential conflict
  createItemInContainer('div', null, '', '7', null, 7, stockAnswersContainerDiv); // nagging
  createItemInContainer('div', null, '', '70', null, 70, stockAnswersContainerDiv); // nagging + reset
  createItemInContainer('div', null, '', '99', null, 99, stockAnswersContainerDiv); // resolving / closing
*/

if ((useStockAnswers == 1) && (task_edit_mode == 0) && (comment_textfield != null)) {

/* TODO: BROKEN:
  if ((priority != null) && (task_status_is_open == 1)) {
    if (priority == "Needs Triage ") {
      function Enhancement (Event) {
        var Text = "Hi @" + reporter + ", thanks for sharing your idea. \nThe title/summary of this report does not describe a software bug but a solution or feature / new functionality / wish. Hence [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | I am setting the priority according to the usual standard for new feature requests ]]. This change doesn't indicate any disagreement with your idea.";
        addTextToComment(Text, 'lowest', '', '');
      }
      createItemInContainer('span', null, 'general_enhancement', '[Lowest+Enhancement]', Enhancement, '70', stockAnswersContainerDiv);

      function SetPriorityLowest (Event) {
        addTextToComment('', 'lowest', '', '');
        var Text = "Setting priority to lowest as per [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | recommendations ]].";
      }
      createItemInContainer('span', null, 'Priority-Lowest', '[Lowest]', SetPriorityLowest, '3', stockAnswersContainerDiv);

      function SetPriorityLow (Event) {
        var Text = "Setting priority to low as per [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | recommendations ]].";
        addTextToComment('', 'low', '', '');
      }
      createItemInContainer('span', null, 'Priority-Low', '[Low]', SetPriorityLow, '3', stockAnswersContainerDiv);

      function SetPriorityNormal (Event) {
        var Text = "Setting priority to normal as per [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | recommendations ]].";
        addTextToComment('', 'normal', '', '');
      }
      createItemInContainer('span', null, 'Priority-Normal', '[Normal]', SetPriorityNormal, '3', stockAnswersContainerDiv);

      function SetPriorityHigh (Event) {
        var Text = "Setting priority to high as per [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | recommendations ]].";
        addTextToComment('', 'high', '', '');
      }
      createItemInContainer('span', null, 'Priority-High', '[High]', SetPriorityHigh, '3', stockAnswersContainerDiv);
    }
  }
*/

  function GenericThanks (Event) {
    var Text = textThanks;
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'generic_thanks', '[Thanks]', GenericThanks, '1', stockAnswersContainerDiv);

  function GenericThanksNew (Event) {
    addTextToComment(textThanksNew, '', '', '');
  }
  createItemInContainer('span', null, 'generic_thanks', '[ThanksNew]', GenericThanksNew, '1', stockAnswersContainerDiv);

  function UseFormsF (Event) {
    var Text = "@" + reporter + ": Thanks for reporting this. For future reference, please use the feature request form (linked from the top of the task creation page) to create feature requests, and fill in the sections in the template. Thanks.\n\n";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'use_formsf', '[Forms: Feature]', UseFormsF, '1', stockAnswersContainerDiv);

  function UseFormsB (Event) {
    var Text = "@" + reporter + ": Thanks for reporting this. For future reference, please use the bug report form (linked from the top of the task creation page) to create a bug report, and fill in the sections in the template. Thanks.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'use_formsb', '[Forms: Bug]', UseFormsB, '1', stockAnswersContainerDiv);

  function Vague (Event) {
    var Text = textThanks + "Unfortunately this Wikimedia Phabricator task lacks some information.\nIf you have time and can still reproduce the situation: Please [add a more complete description to this task](https://www.mediawiki.org/wiki/How_to_report_a_bug). That should be\n* a clear and complete list of exact steps to reproduce the situation, step by step, so that nobody needs to guess or interpret how you performed each step,\n* what happens after performing these steps to reproduce,\n* what you expected to happen instead,\n* a full link to a web address where the issue can be seen,\n* the web browser(s) and web browser version(s) that you tested.\nIn some situations it can also be helpful to know if you see the same behavior when you are not logged in, and your exact MediaWiki version (if it is not on a Wikimedia website) which is shown on `Special:Version`, and your exact PHP version, etc.\n\nYou can edit the task description by clicking {nav icon=pencil, name=Edit Task}. Ideally, a good description should allow any other person to follow these steps (without having to interpret steps) and see the same results. Problems that others can reproduce can get fixed faster. Thanks again!";
    addTextToComment(Text, '', 'STALLED', '');
  }
  createItemInContainer('span', null, 'general_vague', '[Vague]', Vague, '1', stockAnswersContainerDiv);

  if (assignee != "None") {
    function AssigneeSetByReporter (Event) {
      var Text = "@" + reporter + ": I see that you assigned this task to XXXXX. Did you talk to them before, and did they agree that they plan to work on this task?\n(If they did not, then [please remove the assignee](https://www.mediawiki.org/wiki/How_to_report_a_bug) via {nav name=Add Action... > Assign / Claim} in the dropdown menu.) Thanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'AssigneeSetByReporter', '[Assignee:HasAgreed?]', AssigneeSetByReporter, '1', stockAnswersContainerDiv);
  }

  function ErrorConsole (Event) {
    var Text = textThanks + "Could you please try again with `safemode=1`? This allows you to test if a problem is because of your user scripts or personal gadgets without uninstalling them. See https://www.mediawiki.org/wiki/Help:Locating_broken_scripts for more information. \n\nIf this does not solve the problem, please use your web browser's developer tools. After opening them, please append `?debug=true` to the address of the page on which you see the problem, if the address does not already include a `?`. (If the address already includes a `?`, append `&debug=true` instead - see https://www.mediawiki.org/wiki/ResourceLoader/Developing_with_ResourceLoader#Toggle_debug_mode for more information). \nIf there is a problem or an error with [[ https://en.wikipedia.org/wiki/JavaScript | JavaScript ]] it should be printed in the 'console' of the developer tools. Often JavaScript errors are a reason for problems, or a non-existing file called from another file (which can be seen in the 'network' tab). \nFor more information please see:\n* https://en.wikipedia.org/wiki/Wikipedia:Reporting_JavaScript_errors\n* https://developer.mozilla.org/en-US/docs/Tools/Web_Console for Firefox\n* https://docs.microsoft.com/en-us/microsoft-edge/devtools-guide/console for Microsoft Edge\n* https://msdn.microsoft.com/en-us/library/ie/dn255006 for Internet Explorer\n* https://developers.google.com/web/tools/chrome-devtools for Chrome and Chromium\n* https://developer.apple.com/safari/tools/ for Safari\n\nNote: **If** this turns out to be a problem in a user script, gadget or template: \n\nUser scripts, gadgets, templates, custom CSS are local on-wiki content. Local content is managed independently on each wiki, by each wiki community themselves.\nPhabricator is mostly used for MediaWiki, MediaWiki extensions, or server configuration, or by developers and teams to organize what they plan to work on. \nIf you are looking for help with the local code on a wiki, please see https://meta.wikimedia.org/wiki/Small_wiki_toolkits#Ask_your_technical_questions instead.\nThis needs fixing on the local wiki. Hence I am closing this task here - thanks for your understanding!";
    addTextToComment(Text, '', 'STALLED', 'Wikimedia-General-or-Unknown,Community-Tech,#Local-Issues');
  }
  createItemInContainer('span', null, 'general_vague_errorconsole', '[DevTools/Templates]', ErrorConsole, '1', stockAnswersContainerDiv);

  function TooShortDescription (Event) {
    var Text = "Hi @" + reporter + ", the lack of a task description makes is hard for others to help or contribute, for a triager/tester to figure out at some point in the future whether this is still a valid task, for people outside of some team to understand what that team is working on. [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Use_plain_language.2C_define_actions_and_expected_results | Please consider checking the recommendations on task descriptions ]]. Thanks a lot!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'general_short', '[No Desc by Dev]', TooShortDescription, '1', stockAnswersContainerDiv);

  if (product_array.length == 0) {
    function NoProjectAssociatedNewb (Event) {
      var Text = "Hi @" + reporter + ". Can you please associate one (or more) active [[ https://phabricator.wikimedia.org/project/query/G9vp6zKs.IfE/#R | project ]] with this task (via the {nav name=Add Action... > Change Project Tags} dropdown)? This will allow others to get notified, or see this task when searching via projects. Thanks.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'general_short', '[No Proj by Newb]', NoProjectAssociatedNewb, '1', stockAnswersContainerDiv);
  }

  function NoProjectAssociated (Event) {
    var Text = "@" + reporter + ": Assuming this task is about the #XXXXXXXXXX code project, hence adding that project tag so other people who don't know or don't care about team tags can also find this task when searching via projects.  Please set appropriate project tags when possible. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'general_short', '[Adding Proj!]', NoProjectAssociated, '1', stockAnswersContainerDiv);

  function InlinePatchAttached (Event) {
    var Text = "Thanks for taking a look at the code! \n\nYou are very welcome to use [[ https://www.mediawiki.org/wiki/Developer_access | developer access ]] to submit the proposed code changes as a [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Git branch directly into Gerrit ]] which makes it easier to review and provide feedback. If you don't want to set up Git/Gerrit, you can also use the [[ https://gerrit-patch-uploader.toolforge.org/ | Gerrit Patch Uploader ]]. Thanks again!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'patch_attached', '[Patch]', InlinePatchAttached, '1', stockAnswersContainerDiv);

/* TODO: DOES NOT WORK SINCE AJAX
  function BackFromStalledToOpenCreated (Event) {
    var Text = "\n(Please change the status of this task back from \"stalled\" to \"open\" afterwards.)";
    addTextToComment(Text, '', 'STALLED', '');
  }
  createItemInContainer('span', null, 'stalled', '[+Reset status!]', BackFromStalledToOpenCreated, '10', stockAnswersContainerDiv);
*/

  function SolutionInsteadOfProblem (Event) {
    var Text = textThanks + "\nThis task proposes a potential solution. There is currently not enough information [what the underlying problem or use case is which you would like to see solved by this desired solution](https://meta.stackexchange.com/questions/66377/what-is-the-xy-problem#answer-66378). Others might be aware of an already existing (or less obvious) solution, or discuss alternative or better solutions how this could be best solved. \nPlease improve the initial task description by clicking {nav icon=pencil, name=Edit Task}. Also see [[ https://www.mediawiki.org/wiki/How_to_report_a_bug | How to report a bug ]] for more information. Thanks in advance!\n\nTo help developers and product managers prioritize, please provide examples of use cases how you would use this feature if it existed. How would this help people who edit, review, or contribute in other ways to our wikis? Which workflows are currently not possible or would become better? Please note that other solutions might be considered as well.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'solution_without_problem', '[Solution w/o problem]', SolutionInsteadOfProblem, '1', stockAnswersContainerDiv);

  function DupMaybe (Event) {
    var Text = "@" + reporter + ": Is this the same as {Txxxxxx}? If it is, then please feel free to {nav icon=anchor,name=Edit Related Tasks... > Close As Duplicate} in the upper right corner. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'solution_without_problem', '[Dup?]', DupMaybe, '1', stockAnswersContainerDiv);

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

  function OutreachGeneralDevQuestions (Event) {
    var Text = "Hi @XXXLastCommenterXXX. Thanks for your interest! Please see the information for participants on https://www.mediawiki.org/wiki/Outreach_programs and [ask general setup questions in support forums](https://www.mediawiki.org/wiki/New_Developers#Some_general_communication_tips) instead as they are not directly related to fixing this very task. Thanks.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'outreachprograms', '[Outreach: Questions]', OutreachGeneralDevQuestions, '5', stockAnswersContainerDiv);

  function AttachmentTextAsImage (Event) {
    var Text = "Whenever possible, please paste text (such as error messages) as plain text. Text in screenshots or in office documents cannot easily be found by search engines. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'general_text_attachment_as_image', '[Text As Image]', AttachmentTextAsImage, '5', stockAnswersContainerDiv);

  function FileSeparateTask (Event) {
    var Text = "Could you please file a separate task for this? Adding comments to a related task will likely not get a different bug fixed. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'file_seperate_task', '[File Separate Task]', FileSeparateTask, '5', stockAnswersContainerDiv);

  function ScreenshotPlease (Event) {
    var Text = "Could you [add a screenshot](https://www.mediawiki.org/wiki/Phabricator/Help#Uploading_file_attachments) if possible? Thanks.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'ScreenshotPlease', '[Screenshot Please]', ScreenshotPlease, '5', stockAnswersContainerDiv);

  function WhichEditor (Event) {
    var Text = "Which [editor software](https://www.mediawiki.org/wiki/Editor) is this about?";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'whicheditor', '[Which Editor SW?]', WhichEditor, '5', stockAnswersContainerDiv);

  function wikEd (Event) {
    var Text = "Please see https://en.wikipedia.org/wiki/User:Cacycle/wikEd#Bug_reports where to report issues with wikEd. I am closing this task here in Wikimedia Phabricator as wikEd reports do not seem to be tracked and managed here.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'wikEd', '[†wiKEd]', wikEd, '99', stockAnswersContainerDiv);

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

  if (product_array.indexOf('Patch-For-Review') > -1) {
    function PatchReviewPingReply (Event) {
      var Text = "Thanks for the ping! The patch got reviewed in ... but needs rebasing...\n\n[[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Would you fancy doing this ]] in order to help moving forward?\n\nUnfortunately there are always some patches that fall through the cracks, for numerous reasons.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'crping', '[CR Ping: Reply]', PatchReviewPingReply, '3', stockAnswersContainerDiv);
  }

  function WM_SiteConfigurationSetTag (Event) {
    var Text = "Setting project to #Wikimedia-Site-requests, as this request is about settings / configuration of a Wikimedia website. \n\nIn the future, please always follow https://meta.wikimedia.org/wiki/Requesting_wiki_configuration_changes when requesting such site configuration changes. Thanks!\n\nNote: If you are curious and want to try making this configuration change yourself, please follow https://wikitech.wikimedia.org/wiki/Wikimedia_site_requests and give it a try!";
    addTextToComment(Text, '', '', 'Wikimedia-Site-requests');
  }
  createItemInContainer('span', null, 'movetowm', '[→Site-requests]', WM_SiteConfigurationSetTag, '3', stockAnswersContainerDiv);

  function WM_SiteLocalUploads (Event) {
    var Text = "Please also see https://meta.wikimedia.org/wiki/Local_uploads_policy and the missing entry on https://meta.wikimedia.org/wiki/Non-free_content#Exemption_Doctrine_Policy";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'localuploads', '[Local Upload Policy]', WM_SiteLocalUploads, '3', stockAnswersContainerDiv);

  function UncommonEnhancement (Event) {
    var Text = "Hi @" + reporter + ", thanks for sharing your idea! \nCurrently there are no plans to provide this functionality in the code of MediaWiki itself as this idea might be useful for a smaller number of users. When code adds new functionality, the code needs to be supported and tested in all future releases. When underlying code changes in the future, someone has to maintain (fix) the code which according to experience might not always be the original author. You could [[ https://www.mediawiki.org/wiki/Manual:Extensions | turn this idea into a MediaWiki extension ]] which is not maintained by the MediaWiki developers themselves.\nThanks for your understanding!";
    addTextToComment(Text, 'lowest', '', 'MediaWiki-extensions-requests');
  }
  createItemInContainer('span', null, 'general_old_and_retest', '[Uncommon Feature ➞ Ext]', UncommonEnhancement, '3', stockAnswersContainerDiv);

  function WM_SiteConfigurationCommunityConsensusMissing (Event) {
    var Text = "Hi @" + reporter + ". For configuration change, local consensus is required [[ https://meta.wikimedia.org/wiki/Requesting_wiki_configuration_changes | according to 'Requesting wiki configuration changes' ]]. Even if the community is small, an announcement of proposed changes is welcomed. \nCould you bring up the matter on the Village pump page of your wiki (or any other appropriate place) to confirm that this change is wanted by the community, and paste the link to that discussion here?\n\nFor more information about how to request these kinds of changes, please see [[ https://meta.wikimedia.org/wiki/Requesting_wiki_configuration_changes | 'Requesting wiki configuration changes' ]] and feel free to forward that link to others who may want similar changes made in the future. Thanks!\n\n**TODO:** Also, check if this extension needs more localisation first before installing it, e.g.: https://translatewiki.net/w/i.php?title=Special%3ATranslate&taction=translate&group=ext-wikilove&language=ur&limit=1000&task=untranslated";
    addTextToComment(Text, '', 'STALLED', 'community-consensus-needed');
  }
  createItemInContainer('span', null, 'wm_siteconf', '[Consensus?]', WM_SiteConfigurationCommunityConsensusMissing, '4', stockAnswersContainerDiv);

  function WM_TemporarilyRemoveThrottling (Event) {
    var Text = "Thanks for creating this ticket. Please provide all the [[ https://meta.wikimedia.org/wiki/Mass_account_creation#Requesting_temporary_lift_of_IP_cap | required information ]].\n\nIf you think your request has gone unnoticed and the event is about to start, go to the IRC channel #wikimedia-tech on Freenode IRC and ask for help ([[ http://meta.wikimedia.org/wiki/IRC | general information on using IRC ]]).\n\n**TODO:** GO TO EDIT PAGE OF TASK TO REMOVE PREVIOUS PROJECT(S)?";
    addTextToComment(Text, '', '', 'Wikimedia-Site-requests');
  }
  createItemInContainer('span', null, 'throttle_wikimedia', '[IP Throttling]', WM_TemporarilyRemoveThrottling, '4', stockAnswersContainerDiv);

/*
  function EditorCharsPalette (Event) {
    var Text = "Moving to MediaWiki-Extensions-WikiEditor.\n\nIn case you'd like to try fixing this yourself, the code is located at https://gerrit.wikimedia.org/r/#/q/status:merged+project:mediawiki/extensions/WikiEditor,n,z;f=modules/jquery.wikiEditor.toolbar.config.js";
    addTextToComment(Text, '', '', 'MediaWiki-extensions-WikiEditor');
  }
  createItemInContainer('span', null, 'editor_chars_palette', '[Editor Char Palette Bug]', EditorCharsPalette, '5', stockAnswersContainerDiv);
*/

  if ((product_array != null) && (product_array.indexOf('Tracking-Neverending') > -1)) {
    function Tracking2Epic (Event) {
      var Text = "#epic task which could be declared resolved at some point in a far away future, hence removing #tracking-neverending tag.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'projectcreated', '[Tracking🡒Epic]', Tracking2Epic, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Project-Admins') > -1)) {
    function ProjectCreated (Event) {
      var Text = "Requested public project has been created: XXXProjectnameXXX\n\n(In case you need to edit the project or project workboard itself at some point and lack permissions, please see #Trusted-Contributors.)\n\nInterested people are welcome to join the project as {icon users} members, and to [watch the project](https://www.mediawiki.org/wiki/Phabricator/Help#Receiving_updates_and_notifications) in order to receive notifications on task updates.\n\nIf tasks are created under this new project which are about a specific codebase, please make sure to also add these codebase project tags to the tasks in addition.\n\n[Recommended practices for project and workboard management in Phabricator](https://www.mediawiki.org/wiki/Phabricator/Project_management) are available.\n\nFeel free to [[ https://www.mediawiki.org/wiki/Talk:Phabricator/Help | bring up any questions you might have about Phabricator ]] or [[ https://www.mediawiki.org/wiki/Talk:Phabricator/Project_management | about best ways to manage projects in Phabricator ]].\n\nEnjoy!";
      addTextToComment(Text, '', 'RESOLVED', '');
    }
    createItemInContainer('span', null, 'projectcreated', '[Project Created!]', ProjectCreated, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Developer-Wishlist') > -1)) {
    function notadevwish (Event) {
      var Text = "This task is not in scope for the Developer Wishlist as this task is not about a feature that would help development / make a //developer//'s life better/easier.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'notadevwish', '[Not a dev wish!]', notadevwish, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Security') > -1)) {
    function WM_SecurityAddTag (Event) {
      var Text = "Adding codebase project, so developers who also have access to Security tasks could get aware of this task.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_siteconf', '[Sec: Add codebase]', WM_SecurityAddTag, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Cloud-VPS (Project-requests)') > -1)) {
    function CloudVPSRequestOutOfScope (Event) {
      var Text = "Incubating new wiki projects is not a direct scope fit for #Cloud-VPS. \n\nThe purpose sounds a bit broad and the project name implies that this might only be used by you?\n\nWe generally do not grant Cloud VPS projects for single user development use. The reasoning is that #Cloud-VPS virtual machines are a constrained resource, and we do not have the ability to provide a VM for any and all Wikimedia technical contributors for personal use. See https://wikitech.wikimedia.org/wiki/Help:Cloud_VPS_project#Guidelines_for_project_requests for more information.\n\nFor information to set up MediaWiki locally, see https://www.mediawiki.org/wiki/How_to_become_a_MediaWiki_hacker . It is also possible to apply for a grant from the [Foundation's hardware donation program](https://meta.wikimedia.org/wiki/Hardware_donation_program). Another option is to find an existing project that feels the work you are doing is aligned with their goals and make use of that project's existing quota.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'outofscopeforcloudvps', '[Cloud-VPS-OutOfScope]', CloudVPSRequestOutOfScope, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('good first task') > -1)) {
    function RemoveEasyTag (Event) {
      var Text = "@" + last_user_adding_goodfirsttask_tag + ": A #good_first_task is a self-contained, non-controversial task with a clear approach. It should be well-described with pointers to help a completely new contributor. Given the current short task description I'm removing the #good_first_task tag. Please add details what exactly has to happen where and how for a new contributor, and then add back the #good_first_task project tag. Thanks a lot in advance!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'noteasy', '[Not a #good_first_task!]', RemoveEasyTag, '0', stockAnswersContainerDiv);
  }

  function WM_TrustyToolsDeath (Event) {
    var Text = "(due to Trusty migration)\n\n\nThis is intentional: `XXXXX` is listed on https://tools.wmflabs.org/trusty-tools/\n\nNobody has yet migrated `XXXXX` to a supported operating system: `XXXXX` was still running on a machine using an operating system which saw its end of life (no security fixes anymore) in April 2019. See https://wikitech.wikimedia.org/wiki/News/Trusty_deprecation and https://wikitech.wikimedia.org/wiki/News/Toolforge_Trusty_deprecation for more information what needs to be done.";
      addTextToComment(Text, '', '', '');
    }
  createItemInContainer('span', null, 'wm_toolstrustdeath', '[201903 Tools Trusty Death]', WM_TrustyToolsDeath, '0', stockAnswersContainerDiv);

  if ((product_array != null) && (product_array.indexOf('MediaWiki-General') > -1)) {
    function WM_NamespacesWritePatch (Event) {
      var Text = "Just a hint to potentially speed up the process: If you (or anybody else) would like to create a software patch to change these translations, please see\n* [[ https://www.mediawiki.org/wiki/Developer_access | developer access ]]\n* [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Gerrit tutorial ]]\nThe file to change for this request is the corresponding file for this language in the folder https://phabricator.wikimedia.org/diffusion/MW/browse/master/languages/messages/ \nPlease note that [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | providing help with Git/Gerrit ]] is out of scope for Phabricator tasks.\nThanks!";
      addTextToComment(Text, '', '', 'MediaWiki-Internationalization');
    }
    createItemInContainer('span', null, 'mw_namespacespatch', '[Namespaces translation]', WM_NamespacesWritePatch, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && ((product_array.indexOf('MediaWiki-Internationalization') > -1) | (product_array.indexOf('MediaWiki-extensions-CLDR') > -1))) {
    function WM_CheckCLDR (Event) {
      var Text = "As we would like to stay in sync with the definitions in the Unicode Common Locale Data Repository (http://cldr.unicode.org/ and used via https://www.mediawiki.org/wiki/Extension:CLDR), a check would be welcome. This means taking a look at the latest revision of http://unicode.org/cldr/trac/log/trunk/common/main/xy.xml (replace xy with your language code), choosing the appropriate calendar type by searching for \"<calendar type\", and taking a look at the entries in the <dateFormats>, <timeFormats> and <dateTimeFormats> sections.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_checkCLDR', '[Formats: Check CLDR]', WM_CheckCLDR, '0', stockAnswersContainerDiv);

    function WM_Linktrail (Event) {
      var Text = "Anyone feel free to propose a software change (\"patch\") to adjust the [regex](https://en.wikipedia.org/wiki/Regular_expression) of the `$linkTrail` setting in the `MessagesXX.php` file (replace `XX` with the code for your language) in https://phabricator.wikimedia.org/source/mediawiki/browse/master/languages/messages/ .\nSee https://www.mediawiki.org/wiki/Gerrit/Tutorial how to propose a patch.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_linktrail', '[Linktrail]', WM_Linktrail, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && ((product_array.indexOf('UploadWizard') > -1) | (product_array.indexOf('MediaWiki-Uploading') > -1))) {
    function MW_UploadWizard (Event) {
      var Text = "Hi @" + reporter + ".\n\nDoes anything appear in your browser's console when loading the page? Often JavaScript errors are a reason for problems. For more information please see:\n* [[ https://developer.mozilla.org/en-US/docs/Tools/Web_Console | Firefox ]]\n* [[ http://msdn.microsoft.com/en-us/library/ie/dn255006 | Internet Explorer ]]\n* [[ https://developer.chrome.com/devtools | Google Chrome ]]\n* [[ https://developer.apple.com/safari/tools/ | Apple Safari ]]\n* [[ https://help.opera.com/en/latest/advanced/#developerTools | Opera ]]. \n\nCan you please run the upload wizard with the debug option enabled? Add \"?debug=true\" at the end of the web address (after \"Special:UploadWizard\") and then reload the page.\nAlso, do you have the API enabled on your wiki? Is the write API enabled? Are uploads enabled at all?\nFurthermore, I'd like to know your UploadWizard configuration. What do you have in LocalDefaults.php for $wgUploadWizardConfig?\n\nIn your user preferences, under the \nUpload Wizard\" tab (`Special:Preferences#mw-prefsection-uploads`), what is the value of `Maximum number of concurrent uploads`?";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_UW', '[UW: General]', MW_UploadWizard, '0', stockAnswersContainerDiv);

    function MW_UploadNeedinfo (Event) {
      var Text = "What exactly did you add to your LocalSettings.php file to attempt to allow uploading such files?\n\nAlso see [[ https://www.mediawiki.org/wiki/Manual:LocalSettings.php | Manual:LocalSettings.php ]] and [[ https://www.mediawiki.org/wiki/Manual:Configuring_file_uploads | Manual:Configuring_file_uploads ]] and [[ https://www.mediawiki.org/wiki/Manual:%24wgFileExtensions | Manual:%24wgFileExtensions ]] for more information.";
      addTextToComment(Text, '', 'STALLED', '');
    }
    createItemInContainer('span', null, 'mw_uploading', '[Uploading File Types]', MW_UploadNeedinfo, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('MediaWiki-extensions-UniversalLanguageSelector') > -1)) {
    function MW_NewWebfontRequest (Event) {
      var Text = "If you would like to contribute webfonts, please provide patches which will get reviewed. Helpful documentation is available at https://www.mediawiki.org/wiki/Extension:UniversalLanguageSelector#Adding_fonts and [[ https://www.mediawiki.org/wiki/Developer_access | developer access ]].\n\n// TODO: Make task depend on T55015";
      addTextToComment(Text, 'low', '', '');
    }
    createItemInContainer('span', null, 'mw_ULS', '[Webfont request]', MW_NewWebfontRequest, '0', stockAnswersContainerDiv);
  }

  function WM_LoginProblems (Event) {
    var Text = "For anyone affected by log-in problems who wants to help track them down, please see and follow https://www.mediawiki.org/wiki/Manual:How_to_debug/Login_problems and report back here. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'wm_loginproblems', '[Login/Session Problems]', WM_LoginProblems, '4', stockAnswersContainerDiv);

  function WM_PageCount (Event) {
    var Text = "The page `Special:Statistics` lists the number of content pages. The number is updated by running `initSiteStats.php`. `initSiteStats.php` is run at 05:39 UTC on 1st and 15th of each month. According to [configuration](https://noc.wikimedia.org/conf/InitialiseSettings.php.txt), `wgArticleCountMethod` is set to `link` for this wiki. That means that [a page must contain a wikilink to be included in the content page count](https://www.mediawiki.org/wiki/Manual:$wgArticleCountMethod). So the number might be lower than you expected.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'wm_pagecount', '[Common issue: Wrong Page Count]', WM_PageCount, '4', stockAnswersContainerDiv);

  if (product_array.indexOf('Gerrit-Privilege-Requests') > -1) {
    function WM_Gerrit2 (Event) {
      var Text = "As per https://www.mediawiki.org/wiki/Gerrit/Privilege_policy#Requesting_Gerrit_privileges (linked from https://phabricator.wikimedia.org/project/profile/3957/ ), these requests welcome broader input. Could you point to an email on the wikitech-l mailing list please? Thanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_gerrit2', '[Gerrit+2 Req]', WM_Gerrit2, '4', stockAnswersContainerDiv);
  }

  if (product_array.indexOf('Wikimedia-SVG-rendering') > -1) {
    function WM_librsvg (Event) {
      var Text = "As of November 2019, Wikimedia servers have an older librsvg2 version (the one shipped by Debian) on its servers - see T193352 blocked by T216815 (though I'm not sure if the fix for this issue is already in librsvg2 2.44.10 included in Debian Buster).\n\nThumbnails????: Using newer librsvg2 2.46.3 locally and running `rsvg-convert -h 1234567 -f png -o output.png input.svg`, this still shows the same problem. Might be worth to report to upstream at https://gitlab.gnome.org/GNOME/librsvg/issues/";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_librsvg', '[librsvg]', WM_librsvg, '4', stockAnswersContainerDiv);
  }

  if (product_array.indexOf('Wikimedia-Site-requests') > -1) {
    function WM_CustomMobileLogo (Event) {
      var Text = "If you already have a localized wordmark in [SVG file format](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics), please share a direct link to the file or [upload the file here](https://www.mediawiki.org/wiki/Phabricator/Help#Uploading_file_attachments). You or anyone could write a configuration change by following [the Gerrit how-to](https://www.mediawiki.org/wiki/Gerrit/Tutorial). Past examples for such configuration changes are [here](https://gerrit.wikimedia.org/r/#/c/operations/mediawiki-config/+/353012/), [here](https://gerrit.wikimedia.org/r/#/c/operations/mediawiki-config/+/368444/) and [here](https://gerrit.wikimedia.org/r/#/c/operations/mediawiki-config/+/423497/).\n\nIf there is no localized SVG file yet and you would like to create one, please see [[ https://commons.wikimedia.org/wiki/Help:SVG#Software | c:Help:SVG]]. You can also request someone else to create it at [[ https://commons.wikimedia.org/wiki/Commons:Graphic_Lab/Illustration_workshop | c:Commons:Graphic Lab/Illustration workshop]].";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_custommobilelogo', '[Custom Mobile Logo]', WM_CustomMobileLogo, '4', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('MediaWiki-extensions-MultimediaViewer') > -1)) {
    function MW_MultimediaViewer (Event) {
      var Text = textThanks + "When you click on the first link directly, can you access the image and is the image displayed in your web browser? If it is not displayed, what is displayed / which exact error message is shown?\nDoes this problem also happen with another browser (which browser)?\nHave you tried to [[ https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache | bypass your browser cache ]]?";
      addTextToComment(Text, '', 'STALLED', '');
    }
    createItemInContainer('span', null, 'MMV', '[MMV: Broken image]', MW_MultimediaViewer, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && ((product_array.indexOf('Wikimedia-Interwiki-links') > -1) || (product_array.indexOf('MediaWiki-Interwiki') > -1))) {
    function WM_Interwiki_map (Event) {
      var Text = "To update [[ https://www.mediawiki.org/wiki/Special:Interwiki | Wikimedia’s interwiki table ]] please request the change at https://meta.wikimedia.org/wiki/Interwiki_map . This is not handled in Phabricator hence closing this task as invalid.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'MMV', '[Update Interwiki map]', WM_Interwiki_map, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Beta-Cluster-Infrastructure') > -1)) {
    function BetaClusterStewardRightsRequest (Event) {
      var Text = "Could you please point to any code patches that you have contributed in the past, as we'd need to know about your involvement in the technical community? Thanks! --- TODO: Also check trust levels via https://meta.wikimedia.org/wiki/Special:CentralAuth?target=UserName and Phab activity";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'MMV', '[BetaCluster Steward Rights Request]', BetaClusterStewardRightsRequest, '0', stockAnswersContainerDiv);
  }
/*
  if ((product_array != null) && (product_array.indexOf('TechCom-RFC') > -1)) {
    function NotTechComScope (Event) {
      var Text = "As this is currently proposed this is not a technical proposal and does not need a TechCom RFC. It is more of a feature request. To get this done you or someone else would need to write a software change rather than TechCom approval.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'MMV', '[Not in TechCom Scope]', NotTechComScope, '0', stockAnswersContainerDiv);
  }
*/
  if ((product_array != null) && ((product_array.indexOf('WikiEditor') > -1) | (product_array.indexOf('OOUI') > -1))) {
    function WikiEditorLocalizedButtons (Event) {
      var Text = "If you would like to work on this, see https://gerrit.wikimedia.org/r/#/c/435819/ for an example how to add a localized button as an SVG file, and see https://www.mediawiki.org/wiki/Gerrit/Tutorial for how to write software changes. Thanks!\n\n++OOUI";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'MMV', '[LocalizeButtons]', WikiEditorLocalizedButtons, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Wikimedia-Apache-configuration') > -1)) {
    function WM_ApacheWritePatch (Event) {
      var Text = "If you (or anybody else) would like to create a software patch to change the configuration, please see\n* [[ https://www.mediawiki.org/wiki/Developer_access | developer access ]]\n* [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Gerrit tutorial ]]\nThe files to change for this request are in https://phabricator.wikimedia.org/diffusion/OPUP/browse/production/modules/mediawiki/files/apache/sites/ \nPlease note that [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | providing help with Git/Gerrit ]] is out of scope for Phabricator tasks.\nThanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_siteconf', '[Howto: Patch: Apache]', WM_ApacheWritePatch, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('DNS') > -1)) {
    function WM_DNSWritePatch (Event) {
      var Text = "If you (or anybody else) would like to create a software patch to change the configuration, please see\n* [[ https://www.mediawiki.org/wiki/Developer_access | developer access ]]\n* [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Gerrit tutorial ]]\nThe files to change for this request are in https://phabricator.wikimedia.org/diffusion/ODNS/\nPlease note that [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | providing help with Git/Gerrit ]] is out of scope for Phabricator tasks.\nThanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_siteconf', '[Howto: Patch: DNS]', WM_DNSWritePatch, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Wikimedia-IRC') > -1)) {
    function WM_Cloaks (Event) {
      var Text = "List of folks and how to contact: https://meta.wikimedia.org/wiki/IRC_Group_Contacts";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_cloaks', '[IRC: Cloaks]', WM_Cloaks, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf('Wikimedia-Mailing-lists') > -1)) {
    function WM_InternalMailingList (Event) {
      var Text = "Hi @" + reporter + ". Such requests need to be directed at list administrators rather than the operations team. If a user wants to subscribe, the user needs to use the form at https://lists.wikimedia.org/mailman/listinfo/XXXXXXXXXXX in order to contact list administrators but not WMF's operations team.\n\nDepending on the list settings this will subscribe the user or trigger a confirmation email to list admins.  Only if none of the listed admins can actually approve subscription requests, it might be appropriate to ask the operations team to fix access. Proposing to decline this task.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_mailinglist', '[ML: Creation]', WM_SetUpMailingList, '0', stockAnswersContainerDiv);

    function WM_SetUpMailingList (Event) {
      var Text = "Hi @" + reporter + ". Please see https://meta.wikimedia.org/wiki/Mailing_lists#Create_a_new_list for required information. Thanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wmf_internalmailinglist', '[ML: WMF-internal➞Decline]', WM_InternalMailingList, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && ((product_array.indexOf('Wikimedia-Extension-setup') > -1) | (product_array.indexOf('Wikimedia-Language-setup') > -1) | (product_array.indexOf('Wikimedia-Site-requests') > -1))) {
    function WM_ExtensionDeployRequest (Event) {
      var Text = "It looks like this extension has not been deployed on any Wikimedia site yet. Hence it needs to be reviewed first before this request can be fulfilled. Please see https://www.mediawiki.org/wiki/Review_queue for how to proceed.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_extensiondeploy', '[Review/Deploy ext...]', WM_ExtensionDeployRequest, '4', stockAnswersContainerDiv);

    function WM_SiteConfigurationBigFileUpload (Event) {
      var Text = "For future reference, please see https://commons.wikimedia.org/wiki/Help:Server-side_upload for general information.";
      addTextToComment(Text, '', '', 'Wikimedia-Site-requests,Commons');
    }
    createItemInContainer('span', null, 'wm_siteconf', '[Big file upload]', WM_SiteConfigurationBigFileUpload, '4', stockAnswersContainerDiv);

    function WM_SiteConfigurationWritePatch (Event) {
      var Text = "Just a hint: If you (or anybody else) would like to create a software patch to resolve this request, please see [[ https://www.mediawiki.org/wiki/Developer_access | Developer access ]] and the [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | Gerrit tutorial ]]. \nThe files to change for this request are [[ https://phabricator.wikimedia.org/diffusion/OMWC/browse/master/wmf-config/InitialiseSettings.php | InitialiseSettings.php ]] and/or [[ https://phabricator.wikimedia.org/diffusion/OMWC/browse/master/wmf-config/CommonSettings.php | CommonSettings.php ]]. \nPlease note that [[ https://www.mediawiki.org/wiki/Gerrit/Tutorial | providing help with Git/Gerrit ]] is out of scope for Phabricator tasks. Thanks!";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'wm_siteconf', '[Howto: Patch: SiteConfig]', WM_SiteConfigurationWritePatch, '4', stockAnswersContainerDiv);
  }

  if ((product_array != null) && (product_array.indexOf("Cloud-VPS (Quota-requests)") > -1)) {
    function CloudVPS_QuotaRequest (Event) {
      var Text = "For the records, we would like to avoid quota requests which might be in danger of creating a closed membership catch-all project with a single maintainer. Could you clarify what the goal of this project will be and who will be allowed to participate? Thanks!"
      addTextToComment(Text, '', 'STALLED', '');
    }
    createItemInContainer('span', null, 'wmcs_quotarequest', '[CS: QuotaReq-SingleMaint]', CloudVPS_QuotaRequest, '0', stockAnswersContainerDiv);
  }

  if ((product_array != null) && ((product_array.indexOf('Community-Relations') > -1) | (product_array.indexOf('CommRel-') > -1))) {
    function CommRelSupport (Event) {
      var Text = "Community Relations has a little template that's useful for them to gather information around the request for support. Can you please take a look at https://office.wikimedia.org/wiki/Community_Relations#Support and cover those questions? Thanks!"
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'CommRelSupport', '[CommRelSupport]', CommRelSupport, '0', stockAnswersContainerDiv);
  }
  if ((product_array != null) && ((product_array.indexOf('Wikimedia-Portals') > -1) | (product_array.indexOf('CommRel-') > -1))) {
    function WMPortalChangeTagline (Event) {
      var Text = "For the records, this needs to be updated in https://meta.wikimedia.org/wiki/Www.wikiversity.org_template and https://meta.wikimedia.org/wiki/Module:Project_portal/wikis"
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'WikimediaPortals', '[WM Portals: Change Tagline]', WMPortalChangeTagline, '0', stockAnswersContainerDiv);
  }
  function WM_Reset2FA_SUL (Event) {
    var Text = "Hi @" + reporter + ". Could you please send an email (to ca at wikimedia dot org) from your registered email address of your wiki account, in order to confirm? Please also include the URL of this ticket in your email. Thanks!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'wm_reset2FA_SUL', '[Reset 2FA SUL]', WM_Reset2FA_SUL, '0', stockAnswersContainerDiv);

  function WM_Reset2FA_LDAP (Event) {
    var Text = "Hi @" + reporter + ". Please follow https://wikitech.wikimedia.org/wiki/Password_and_2FA_reset#For_users to verify your identity for a Wikitech account 2FA reset.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'wm_reset2FA_SUL', '[Reset 2FA LDAP]', WM_Reset2FA_LDAP, '0', stockAnswersContainerDiv);

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

  if (priority != null) {
    function PriorityWhyRaised (Event) {
      var Text = "Do you [plan to work on fixing this task](https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities), as you [increased the priority](https://www.mediawiki.org/wiki/Bug_management/Phabricator_etiquette) of this task?\n\n\nYou are welcome to increase priority and set yourself as task assignee if you [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities| plan to work on fixing this ]], as [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | the Priority field summarizes and reflects reality and does not cause it ]]. If you're interested in contributing code to fix this, please refer to https://www.mediawiki.org/wiki/How_to_become_a_MediaWiki_hacker - thanks in advance for your help!\nIf you do not plan to work on this task yourself but feel that this task has become more urgent than other tasks on the project workboard and that those with the actual power to put the task on their agenda should act, please discuss with the responsible developers, product managers and budget holders. Resources of teams/developers are limited when it comes to working on requests. We want to be realistic about communicating what is being worked on, to maximize the impact of changes. Practically, this often unfortunately means assigning a lower priority to many tasks. You could also propose this task for the [[ https://meta.wikimedia.org/wiki/Community_Wishlist_Survey | next Community Wishlist survey ]]. Thanks for your understanding!";
        addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'prioraised', '[Explain Prio raise]', PriorityWhyRaised, '6', stockAnswersContainerDiv);

    function PriorityMetaDiscussion (Event) {
      var Text = "[[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | Priority setting is explained here ]]. Anybody (whether volunteer or paid by some organization) is welcome to work on tasks they are interested to see fixed. \n\nBugs, oversights, and unexpected situations do happen in software development. Thanks for your understanding.";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'priometadiscussion', '[Prio Meta Disc.]', PriorityMetaDiscussion, '6', stockAnswersContainerDiv);
  }

  if ((priority != null) & (priority == "Unbreak Now! ")) {
    function PriorityWhyUnbreakNow (Event) {
      var Text = "This task was set to \"Unbreak Now\" priority.\nThis priority is [[ https://www.mediawiki.org/wiki/Phabricator/Project_management#Setting_task_priorities | reserved for urgent issues that require people to drop anything else what they're currently doing ]] and address this problem immediately.\nThat does not seem to be the case here, hence I am lowering the priority again.\n\nIf you disagree with the urgency of this task, replying to the previous arguments by providing technical arguments is more welcome than [[ https://www.mediawiki.org/wiki/Bug_management/Phabricator_etiquette | changing the task priority without providing reasons ]].\n(((((((\"One more affected user\" does not make tasks more urgent. :) ))))))";
      addTextToComment(Text, '', '', '');
    }
      createItemInContainer('span', null, 'unbreak_prio', '[Reset Unbreak now]', PriorityWhyUnbreakNow, '60', stockAnswersContainerDiv);
  }

  function FeatureRequestDissentNeedsArguments (Event) {
    var Text = "If there are convincing arguments how this negatively impacts your workflows and why that impact is big enough that it justifies the additional maintenance costs / resources to long-term maintain additional code, I am pretty sure that developers would be open to discuss such arguments. \nHowever, quoting from https://lwn.net/Articles/712215/ : \"There is a widespread assumption that maintainers are obligated to respond to every question or pull request that comes their way; we should experiment with that assumption. Maintainers should be able to say that a project is simply not accepting contributions, or to limit contributors to a small, known group of developers. Users, of course, will always retain the right to fork the project should they wish to maintain it in a different manner.\"";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'disense_maintainers', '[Disense with maint\'s / long-term costs]', FeatureRequestDissentNeedsArguments, '60', stockAnswersContainerDiv);

  function NonConstructiveComment (Event) {
    var Text = "This contribution does not seem to be helpful to move this task closer to resolution. \nEven if you may have reasons to be annoyed, there is no reason to be confrontational. Explaining upfront the reasons why you are annoyed is more effective. If you do not wish to contribute in a positive way to this discussion, I suggest you spend your time on other topics. Thank you for considering!\n\n\n[[ https://en.wikipedia.org/wiki/Wikipedia:Arguments_to_avoid_on_discussion_pages#Personal_point_of_view | Expressing personal/subjective preferences ]] that do not refer to any specific points being made earlier in the discussion (e.g. the elaboration why and how specific design decisions were made) do not help moving a discussion forward.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'nonconstructive_comment', '[Non-constructive]', NonConstructiveComment, '60', stockAnswersContainerDiv);

  function InvalidAsProtest (Event) {
    var Text = "Closing a task as Invalid or Declined does not resolve an ongoing discussion when the project maintainer(s) think that the task is sensible and valid. If there are open question, please ask them and let's find answers together.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'invalid_as_protest', '[Invalid as protest]', InvalidAsProtest, '60', stockAnswersContainerDiv);

  function DoNotPlus1 (Event) {
    var Text = "Thanks for your interest. If this issue matters to you, then an effective way to express this is to click {nav icon=trophy, name=Award Token}. Writing a non-technical comment is not effective and does not move the conversation on: the more comments, the longer it takes to read the entire ticket, so it will be less likely for developers to take a look or find time to read. Shorter threads are easier to approach and as such more likely to be addressed. Thanks for your understanding.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'plus1', '[Do not +1]', DoNotPlus1, '60', stockAnswersContainerDiv);

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

  function NoResponseYetSorry (Event) {
    var Text = textThanks + "Sorry that nobody has taken a look at this task yet and given feedback.\n";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'general_old_no_response_yet', '[Old:Sorry, no Response]', NoResponseYetSorry, '2', stockAnswersContainerDiv);

  function OldPleaseRetest (Event) {
    var Text = "@" + reporter + ": There has not been any activity in this task recently. Due to the high number of tasks we sometimes have to clean some older tasks, as some might have already been resolved in the meantime in newer versions. \n\nIf you have time, could you please update to a [[ https://www.mediawiki.org/wiki/Version_lifecycle | recent supported version ]] and tell us if this problem still happens?\nIf this still happens, please add a comment to this Phabricator task and tell us either your exact software version in case that you know (for a MediaWiki installation, see the \"Special:Version\" page on your wiki), or the complete web address for a public wiki where the problem can be seen. \n\nIf the problem does not happen anymore at all, please set the status of this task to \"Declined\" via the {nav name=Add Action... > Change Status} dropdown. Thanks a lot for your help!";
    addTextToComment(Text, '', 'STALLED', '');
  }
  createItemInContainer('span', null, 'general_please_retest', '[Old:Please Retest]', OldPleaseRetest, '2', stockAnswersContainerDiv);

  function PleaseReplyToLastComment (Event) {
    var Text = "@" + reporter + ": Could you please answer the last comment(s)? Thanks in advance!";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'general_please_retest', '[Reporter: ping]', PleaseReplyToLastComment, '7', stockAnswersContainerDiv);

  function PastDueDate (Event) {
    if (assignee != "None") {
      var Text = "@" + assignee + ": Hi, the `Due Date` set for this open task passed a while ago.\nCould you please either update or reset the Due Date (by clicking {nav icon=pencil, name=Edit Task}), or set the status of this task to `resolved` in case this task is done? Thanks!";
      addTextToComment(Text, '', '', '');
    }
    else {
      var Text = "@" + reporter + ": Hi, the `Due Date` set for this open task passed a while ago.\nCould you please either update or reset the Due Date (by clicking {nav icon=pencil, name=Edit Task}), or set the status of this task to `resolved` in case this task is done? Thanks!";
      addTextToComment(Text, '', '', '');
    }
  }
  createItemInContainer('span', null, 'past_due_date', '[Update Past Due Date]', PastDueDate, '7', stockAnswersContainerDiv);

  function ResetStalled (Event) {
    var Text = "@" + last_user_changing_task_status + ": The previous comments don't explain who or what (task?) exactly this task is stalled on ([\"If a report is waiting for further input (e.g. from its reporter or a third party) and can currently not be acted on\"](https://www.mediawiki.org/wiki/Bug_management/Bug_report_life_cycle)). Hence resetting task status, as tasks should not be stalled (and then potentially forgotten) for years for unclear reasons.\n\n//(Smallprint, as general orientation for task management:\nIf you wanted to express that nobody is currently working on this task, then the assignee should be removed and/or priority could be lowered instead.\nIf work on this task is blocked by another task, then that other task should be added via {nav name=Edit Related Tasks... > Edit Subtasks}.\nIf this task is stalled on an upstream project, then the #upstream tag should be added.\nIf this task requires info from the task reporter, then there should be instructions which info is needed.\nIf this task needs retesting, then the #testme tag should be added.\nIf this task is out of scope and nobody should ever work on this, or nobody else managed to reproduce the situation described here, then it should have the \"Declined\" status.\nIf the task is valid but should not appear on some team's workboard, then the team project tag should be removed while the task has another active project tag.)//";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'reset_stalled', '[Stalled🡒Open]', ResetStalled, '7', stockAnswersContainerDiv);

  if ((priority != null) & (priority == "High ")) {
    function PriorityHighForLongTime (Event) {
      var Text = "This has been high priority (set by XXXX) for XXXX months now. \n\nEither this task is not high priority and priority should be reset to something more realistic, or someone needs to get set as assignee.";
      addTextToComment(Text, '', '', '');
    }
      createItemInContainer('span', null, 'high_prio', '[HighPrio: Ping!]', PriorityHighForLongTime, '7', stockAnswersContainerDiv);
    }

  if (assignee != "None") {
    createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

    function AssigneeUpdatesRequest (Event) {
      var Text = "@" + assignee + ": Hi! This task has been assigned to you a while ago. Could you maybe share an update? Do you still plan to work on this task, or [do you need any help](https://www.mediawiki.org/wiki/New_Developers/Communication_tips)?\n\nIf this task has been resolved in the meantime: Please update the task status (via {nav name=Add Action... > Change Status} in the dropdown menu).\nIf this task is not resolved and only if you do not plan to work on this task anymore: Please consider removing yourself as assignee (via {nav name=Add Action... > Assign / Claim} in the dropdown menu): That would allow others to work on this (in theory), as others won't think that someone is already working on this. Thanks! :)";
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'AssigneeUpdate', '[Assignee:Status?]', AssigneeUpdatesRequest, '7', stockAnswersContainerDiv);

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

    function AssigneeResetToDefault (Event) {
      var Text = "@" + assignee + ": I am resetting the assignee of this task because there has not been progress lately (please correct me if I am wrong!). Resetting the assignee avoids the impression that somebody is already working on this task. It also allows others to potentially work towards fixing this task. Please claim this task again when you plan to work on it (via {nav name=Add Action... > Assign / Claim} in the dropdown menu) - it would be welcome. Thanks for your understanding!";
      addTextToComment(Text, '', 'assignee_reset', ''); // TODO: Actually do remove the assignee.
    }
    createItemInContainer('span', null, 'AssigneeResetDefault', '[Assignee:Reset!]', AssigneeResetToDefault, '70', stockAnswersContainerDiv);
  }

  createItemInContainer('br', null, null, null, null, null, stockAnswersContainerDiv);

  function SupportRequestAndInvalidHere (Event) {
    var Text = "Hi @" + reporter + ". This does not sound like something is wrong in the code base (a so-called \"software bug\"), but instead like a support request (how to change settings, questions how to do something, etc.). \nAs Wikimedia Phabricator is for bug reports, enhancement requests, and planning work, please check https://www.mediawiki.org/wiki/Communication - Thanks for your understanding!";
    addTextToComment(Text, '', 'INVALID', '');
  }
  createItemInContainer('span', null, 'mw_supportrequest', '[†Support]', SupportRequestAndInvalidHere, '99', stockAnswersContainerDiv);

  function SplitIntoSeveralTasks (Event) {
    var Text = textThanks + "Can you please check https://www.mediawiki.org/wiki/How_to_report_a_bug and split this into separate tasks, to cover only one topic per task?\n\nThat would allow to have more specific task summaries which allows distinguishing when looking at a list of tasks, and it will also allow to track the progress and status of each topic. Hence I am closing this current task as invalid. Thanks for your understanding!";
    addTextToComment(Text, '', 'INVALID', '');
  }
  createItemInContainer('span', null, 'split_into_several_tasks', '[†Split]', SplitIntoSeveralTasks, '99', stockAnswersContainerDiv);

  function UserCannotReproduceAnymore (Event) {
    var Text = "As the issue described in this task cannot be reproduced anymore and as no code was changed in the codebase, I'm [[ https://www.mediawiki.org/wiki/Bug_management/Bug_report_life_cycle | closing this task as declined. ]]";
    addTextToComment(Text, '', 'DECLINED', '');
  }
  createItemInContainer('span', null, 'mw_supportrequest', '[†Cant Repro]', UserCannotReproduceAnymore, '99', stockAnswersContainerDiv);

  function ObsoleteUnsupportedVersion (Event) {
    var Text = textThanks + "\nHowever, you are using a version which is [[ https://www.mediawiki.org/wiki/Version_lifecycle | too old and not supported anymore ]]. Developers are no longer working on that version, so unfortunately there will not be any bug fixes by developers for the version that you use.\nBy [[ https://www.mediawiki.org/wiki/Manual:Upgrading | upgrading ]] to a [[ https://www.mediawiki.org/wiki/Download | newer version of MediaWiki ]] you will receive bug fixes, security updates, and new functionality.\n\nIf the problem still occurs with a newer and supported version of MediaWiki, please set the status of this Phabricator task back to \"Open\" via the {nav name=Add Action... > Change Status} dropdown, provide exact version information, and follow https://www.mediawiki.org/wiki/How_to_report_a_bug . Thanks!";
    addTextToComment(Text, '', 'DECLINED', '');
  }
  createItemInContainer('span', null, 'general_obsolete', '[†Obsolete]', ObsoleteUnsupportedVersion, '99', stockAnswersContainerDiv);

  function EmptyOrTestOnlyTask (Event) {
    var Text = "@" + reporter + ": Thank you for using Wikimedia Phabricator.\nphabricator.wikimedia.org is a task tracking system used by the Wikimedia community to develop [[ https://en.wikipedia.org/wiki/MediaWiki | MediaWiki ]] and other software behind [[ https://www.wikimedia.org/ | Wikimedia sites ]]. It is not a test system to play with. As written on https://phabricator.wikimedia.org/ , please use https://phab.wmflabs.org/ if you want to test Phabricator. \n(//If// you heavily misuse phabricator.wikimedia.org your Wikimedia Phabricator account might be disabled.)\n\nIf you really wanted to write a valid bug report or feature request for Wikimedia, please [[ https://www.mediawiki.org/wiki/How_to_report_a_bug | read the documentation ]] and then create a new Phabricator task. Thanks a lot!";
    addTextToComment(Text, '', 'INVALID', '');
  }
  createItemInContainer('span', null, 'general_empty_report', '[†Test/Spam]', EmptyOrTestOnlyTask, '99', stockAnswersContainerDiv);

  function CloseAsIncomplete (Event) {
    var Text = "Unfortunately closing this Phabricator task as no further information has been provided.\n\n@" + reporter +": After you have provided the information asked for and if this still happens, please set the status of this task back to \"Open\" via the {nav name=Add Action... > Change Status} dropdown. Thanks!";
    addTextToComment(Text, '', 'DECLINED', '');
  }
  createItemInContainer('span', null, 'general_incomplete', '[†WorksForMe]', CloseAsIncomplete, '99', stockAnswersContainerDiv);

  function FixedButNotDeployedComment (Event) {
    var Text = "This is fixed in the codebase, hence closing again. See the [[ https://www.mediawiki.org/wiki/Bug_management/Bug_report_life_cycle | bug report life cycle ]] for the meaning of resolved and see the [[ https://www.mediawiki.org/wiki/MediaWiki_1.31/Roadmap | Roadmap ]] for the dates when you can expect the fix to be available (\"deployed\") on Wikimedia servers ([[ https://wikitech.wikimedia.org/wiki/Deployments | more information on deployments ]]).";
    addTextToComment(Text, '', 'RESOLVED', '');
  }
  createItemInContainer('span', null, 'FixedButNotDeployedComment', '[†Fixed w/o deployed]', FixedButNotDeployedComment, '99', stockAnswersContainerDiv);

  function WikipediaArticleContent (Event) {
    var Text = "\nCould you please discuss the Wikipedia article(s) that you refer to on their corresponding Talk/Discussion page? ([[ https://en.wikipedia.org/wiki/Help:Editing | More information on editing Wikipedia articles is available (on English Wikipedia). ]]) \nFor more general discussion of the content in Wikipedia articles, please refer to https://en.wikipedia.org/wiki/Wikipedia:Help_desk (if this is about English Wikipedia) or the corresponding help desk on the wiki.\n\nClosing this task as [Wikimedia Phabricator](https://www.mediawiki.org/wiki/Phabricator) is for reporting software issues and not for on-wiki content issues. Thanks for your understanding!";
    addTextToComment(Text, '', 'INVALID', '');
  }
  createItemInContainer('span', null, 'WPContent', '[†WP content]', WikipediaArticleContent, '99', stockAnswersContainerDiv);

  function WM_TranslationsAreOnTWN (Event) {
    var Text = "Hi @" + reporter + ". The problem in this ticket is valid, but it is nothing to solve in the code. The translations of the strings must be changed. This can be done by any user who knows the language and you do not need to know programming:\n\nTranslations are done on the translatewiki.net website. You can create an account there, change translations, and after a few days they will be updated automatically on Wikimedia websites.\n\nFor more information, see https://www.mediawiki.org/wiki/Translatewiki.net - thanks!";
    addTextToComment(Text, '', 'INVALID', '');
  }
  createItemInContainer('span', null, 'translations_twn', '[†Transl➞TWN]', WM_TranslationsAreOnTWN, '99', stockAnswersContainerDiv);

  if ((product_array != null) && (product_array.indexOf('Community-consensus-needed') > -1)) {
    function WM_SiteConfigurationCommunityConsensusNeverProvided (Event) {
      var Text = "Declining this request after months of no updates. Assuming there currently is not sufficient [[ https://meta.wikimedia.org/wiki/Requesting_wiki_configuration_changes | community consensus ]]. Please feel free to reopen this task (by setting the status of this Phabricator task back to \"Open\" via the {nav name=Add Action... > Change Status} dropdown) once community agreement is found. Thank you for your understanding!";
      addTextToComment(Text, '', 'INVALID', '');
    }
    createItemInContainer('span', null, 'wm_mailinglist', '[†No Community Consensus]', WM_SiteConfigurationCommunityConsensusNeverProvided, '99', stockAnswersContainerDiv);
  }
  function CanClose (Event) {
    var Text = "If there is nothing left to do here, please feel free to resolve this task via the {nav name=Add Action... > Change Status} dropdown. Thanks.";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'mw_canclose', '[†Resolve?]', CanClose, '99', stockAnswersContainerDiv);

  function BZTrackingTask (Event) {
    var Text = "This ticket does not seem to have been actively used for tracking something in the last six years. /// Closing this inactive incomplete [\"tracking\" task](https://www.mediawiki.org/wiki/Phabricator/Project_management/Tracking_tasks) as a more complete list of tasks in this area can be found by searching for tasks with #technical-debt and #TODOTheProjectTag instead. \n\nClosing as this #Tracking-Neverending task only had and has one subtask, hence it's not \"[tracking](https://www.mediawiki.org/wiki/Phabricator/Project_management/Tracking_tasks)\" anything.\nFeel free to schedule its one subtask instead, if someone has plans to look into this topic. Thanks.\n\nClosing.\n\nThis tracking task is no longer helpful. There are tasks for all remaining work.\n\nAnyone interested can query for tickets with the name of the extension plus the #Wikimedia-Site-requests tag at https://phabricator.wikimedia.org/maniphest/query/advanced/";
    addTextToComment(Text, '', '', '');
  }
  createItemInContainer('span', null, 'bz_tracking', '[†BZ Tracking]', BZTrackingTask, '99', stockAnswersContainerDiv);

if ((product_array != null) && ((product_array.indexOf('Wikimedia-Technical-Conference-2021') > -1) | (product_array.indexOf('Wikimania-Hackathon-2021') > -1) | (product_array.indexOf('Wikimedia-Hackathon-2022') > -1) | (product_array.indexOf('Wikimania-Hackathon-2022') > -1))) {

    function CanCloseConferenceProject (Event) {
      if (assignee != "None") {
        var Text = "@" + assignee + ": Thanks for participating in the Hackathon! We hope you had a great time.\n\n* If this task was being worked on and resolved at the Hackathon: Please change the task status to `resolved` via the {nav name=Add Action... > Change Status} dropdown, and make sure that this task has a link to the public codebase.\n* If this task is still valid and should stay open: Please add another active project tag to this task, so others can find this task (as likely nobody in the future will look back at the Hackathon workboard when trying to find something they are interested in).\n* In case there is nothing else to do for this task, or nobody plans to work on this task anymore: Please set the task status to `declined`.\n\nThank you,\nyour Hackathon venue housekeeping service";
      } else {
        var Text = "@" + reporter + ": Thanks for participating in the Hackathon! We hope you had a great time.\n\n* If this task was being worked on and resolved at the Hackathon: Please change the task status to `resolved` via the {nav name=Add Action... > Change Status} dropdown, and make sure that this task has a link to the public codebase.\n* If this task is still valid and should stay open: Please add another active project tag to this task, so others can find this task (as likely nobody in the future will look back at the Hackathon workboard when trying to find something they are interested in).\n* In case there is nothing else to do for this task, or nobody plans to work on this task anymore: Please set the task status to `declined`.\n\nThank you,\nyour Hackathon venue housekeeping service";
      }
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_cancloseconference', '[Hackathon followup - project: please close/update]', CanCloseConferenceProject, '60', stockAnswersContainerDiv);

    function CanCloseConferenceSession (Event) {
      if (assignee != "None") {
        var Text = "@" + assignee + ": Thanks for participating in the Hackathon! We hope you had a great time.\n\n* If this session / event took place: Please change the task status to `resolved` via the {nav name=Add Action... > Change Status} dropdown.\n** If there are session notes (e.g. on Etherpad or a wiki page), or if the session was recorded, please make sure these resources are linked from this task.\n** If there are specific follow-up tasks from this session / event: Please create dedicated tasks and add another active project tag to those tasks, so others can find those tasks (as likely nobody in the future will look at the Hackathon workboard when trying to find something they are interested in).\n* In this session / event did not take place: Please set the task status to `declined`.\n\nThank you,\nyour Hackathon venue housekeeping service";
      } else {
        var Text = "@" + reporter + ": Thanks for participating in the Hackathon! We hope you had a great time.\n\n* If this session / event took place: Please change the task status to `resolved` via the {nav name=Add Action... > Change Status} dropdown.\n** If there are session notes (e.g. on Etherpad or a wiki page), or if the session was recorded, please make sure these resources are linked from this task.\n** If there are specific follow-up tasks from this session / event: Please create dedicated tasks and add another active project tag to those tasks, so others can find those tasks (as likely nobody in the future will look at the Hackathon workboard when trying to find something they are interested in).\n* In this session / event did not take place: Please set the task status to `declined`.\n\nThank you,\nyour Hackathon venue housekeeping service";
      }
      addTextToComment(Text, '', '', '');
    }
    createItemInContainer('span', null, 'mw_cancloseconference', '[Hackathon followup - session: please close/update]', CanCloseConferenceSession, '60', stockAnswersContainerDiv);
  }
}

/****************************************************************************/
/* Reports page fun:                                                        */
/* - Show percentages per priority instead of total numbers                 */
/* - Show small arrows symbolizing the priority as columns suck             */
/* - Color values if they are higher than a certain threshold               */
/* https://phabricator.wikimedia.org/maniphest/report/project/?order=total  */
/****************************************************************************/
/* Following code is broken and not useful until https://phabricator.wikimedia.org/T125357 is fixed */
if ((window.location.pathname.split( '/' )[2] == "report") && (window.location.pathname.split( '/' )[3] == "project")) {
  var trs = document.getElementsByTagName("tr");
  for (var w = 1; w < trs.length; w++) { // because 0 is <th>
    var tds = trs[w].getElementsByTagName("td");
    var ttl = (tds[7].innerHTML); // row 7 has total number of open tasks
    var regex = new RegExp(',', 'g');
    ttl = ttl.replace(regex, '');
    ttl = parseInt(ttl, 10);
    for (var no = 1; no < 7; no++) { // row 0 is name of project
      var foo = parseInt(tds[no].innerHTML);

      var prio = null; // append symbol for priority because table columns aren't static
      switch (no) {
        case 1: prio = "↑"; break;
        case 2: prio = "?"; break;
        case 3: prio = "↗"; break;
        case 4: prio = "→"; break;
        case 5: prio = "↘"; break;
        case 6: prio = "↓"; break;
      }

      if (Number.isInteger(foo)) {
        var prcnt = (foo / ttl) * 100;
        tds[no].innerHTML = prcnt.toFixed(0) + "%" + prio; // cut digits via toFixed; append priority arrow

        if (((no == 1) || (n == 3)) && (prcnt > 40)) { // for unbreak/high prio; mark large values red
          tds[no].setAttribute('style', "color:#FA1D2F; background-color:#FFDDDD;");
        }
        else if ((no == 2) && (prcnt > 40)) { // for untriage prio; mark large values violet
          tds[no].setAttribute('style', "color:#800080; background-color:#FAD1FA;");
        }
        else if ((no == 4) && (prcnt > 50)) { // for normal prio; mark large values orange
          tds[no].setAttribute('style', "color:#FF7F00; background-color:#FEE5AC;");
        }
      }
    }
  }
}
else if (window.location.pathname.split( '/' )[2] == "report") {
  var product_array_reports=[]; // collect string names of all the Projects listed in this report:
  // "sorted-column" turns into "aphront-table-view-sortable" when sorting, hence using "wide":
  for(i=0; i<((document.getElementsByClassName('wide')).length); i++) {
    product_array_reports[i] = document.getElementsByClassName('wide')[i].textContent;
  }
  if (setColorForExtensions == 1) {
    var product_array_containers_reports=[]; // collect containers of all the Projects in this report task so we can color them:
    for(i=0; i<((document.getElementsByClassName('wide')).length); i++) {
      product_array_containers_reports[i] = document.getElementsByClassName('wide')[i];
    }
  }
}
